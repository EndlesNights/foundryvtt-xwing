# X-Wing Miniatures game system for Foundry VTT (unofficial)

This game system is for playing the X-Wing Miniatures game by FFG in Foundry VTT.  

There's no connection between this game system and Fantasy Flight Games, this is an entirely fan-made system which supports the fundamental gameplay mechanics of the game.  No game material itself is included in the system, so you will need to create your own actors (ships) and items (upgrades) based on game components you own.  

This is done to respect Fantasy Flight Games' [Guidelines for Community Use of Intellectual Property](https://images-cdn.fantasyflightgames.com/filer_public/fa/b1/fab15a15-94a6-404c-ab86-6a3b0e77a7a0/ip_policy_031419_final_v21.pdf).

This system is *very* much a work in progress.  It currently supports the very bare minimum of functionality.

## Features

* X-Wing specific dice rolling, with `Nda` for rolling N attack dice and `Ndd` for rolling N defense dice
* Actor sheets with basic functionality
    * Automatic point total calculation based on upgrades
    * Buttons for Focus/Evade/Boost/Barrel Roll actions (if the ship has those actions)
    * Counters for various tokens like stress/focus/evade
    * Upgrade slot handling with counts
* Secondary actor sheet for editing actors with the key functionality supported (still needs lots of polish)
    * Assign ship type, attack/agility/etc values
    * Add/remove upgrade slots
    * Toggle action bar actions
    * Configure the available maneuvers
* Combat tracker modified to handle X-Wing Minis combat order
    * Handles Assign/Move/Attack phases and re-sorting initiative as-needed
* Maneuver selection dialog that can select and execute maneuvers

## Planned future features

* Too much to bother listing yet.  This is still very much WIP

## Caveats 

* Token movement is currently done via Ray, which plots a line from the token's starting point to the destination post-maneuver.  This works for getting a token from point A to point B, but it doesn't respect RAW token movement (or bumping).  If anyone can think of a good solution, file let me know, because it's currently a known issue/shortcoming that I hope to eventually address.  
* There's no automation to combat.  You'll need to roll attack dice, roll defense dice, and edit the token's shields/hull as-needed to resolve damage.  I hope to address that eventually, but it didn't make it into 0.1.0.  

# Acknowledgements

Thanks to \@Atropos#3814 for creating Foundry VTT in the first place, without which none of this would be possible.

Thanks to \@asacolips#1867 for creating the Boilerplate system as a template and the [tutorial](https://foundryvtt.wiki/en/development/guides/SD-tutorial/SD01-Getting-started) I followed to get started.

Thanks to many users on the Foundry Discord that offered advice, code snippets, and commiseration as I've worked on this game system.
