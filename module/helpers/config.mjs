export const XWING = {};


let ship_list = {
  'awing': {
    'name': 'XWING.ships.awing',
    'icon': '<i class="xwing-ship">a</i>'
  },
  'bwing': {
    'name': 'XWING.ships.bwing',
    'icon': '<i class="xwing-ship">b</i>'
  },
  'ewing': {
    'name': 'XWING.ships.ewing',
    'icon': '<i class="xwing-ship">e</i>'
  },
  'hwk290': {
    'name': 'XWING.ships.hwk290',
    'icon': '<i class="xwing-ship">h</i>'
  },
  'kwing': {
    'name': 'XWING.ships.kwing',
    'icon': '<i class="xwing-ship">k</i>'
  },
  'scurrg': {
    'name': 'XWING.ships.scurrg',
    'icon': '<i class="xwing-ship">H</i>'
  },
  'xwing': {
    'name': 'XWING.ships.xwing',
    'icon': '<i class="xwing-ship">x</i>'
  },
  'yt1300': {
    'name': 'XWING.ships.yt1300',
    'icon': '<i class="xwing-ship">m</i>'
  },
  'yt2400': {
    'name': 'XWING.ships.yt2400',
    'icon': '<i class="xwing-ship">o</i>'
  },
  'ywing': {
    'name': 'XWING.ships.ywing',
    'icon': '<i class="xwing-ship">y</i>'
  },
  'z95': {
    'name': 'XWING.ships.z95',
    'icon': '<i class="xwing-ship">z</i>'
  },

  'decimator': {
    'name': 'XWING.ships.decimator',
    'icon': '<i class="xwing-ship">d</i>'
  },
  'firespray': {
    'name': 'XWING.ships.firespray',
    'icon': '<i class="xwing-ship">f</i>'
  },
  'lambda': {
    'name': 'XWING.ships.lambda',
    'icon': '<i class="xwing-ship">l</i>'
  },
  'tie_advanced': {
    'name': 'XWING.ships.tie_advanced',
    'icon': '<i class="xwing-ship">A</i>'
  },
  'tie_bomber': {
    'name': 'XWING.ships.tie_bomber',
    'icon': '<i class="xwing-ship">B</i>'
  },
  'tie_defender': {
    'name': 'XWING.ships.tie_defender',
    'icon': '<i class="xwing-ship">D</i>'
  },
  'tie_fighter': {
    'name': 'XWING.ships.tie_fighter',
    'icon': '<i class="xwing-ship">F</i>'
  },
  'tie_interceptor': {
    'name': 'XWING.ships.tie_interceptor',
    'icon': '<i class="xwing-ship">I</i>'
  },
  'tie_phantom': {
    'name': 'XWING.ships.tie_phantom',
    'icon': '<i class="xwing-ship">P</i>'
  },
  'tie_punisher': {
    'name': 'XWING.ships.tie_punisher',
    'icon': '<i class="xwing-ship">N</i>'
  },

  'aggressor': {
    'name': 'XWING.ships.aggressor',
    'icon': '<i class="xwing-ship">i</i>'
  },
  'g1a': {
    'name': 'XWING.ships.g1a',
    'icon': '<i class="xwing-ship">n</i>'
  },
  'm3a': {
    'name': 'XWING.ships.m3a',
    'icon': '<i class="xwing-ship">s</i>'
  },
  'starviper': {
    'name': 'XWING.ships.starviper',
    'icon': '<i class="xwing-ship">v</i>'
  },
  'yv666': {
    'name': 'XWING.ships.yv666',
    'icon': '<i class="xwing-ship">t</i>'
  }
}

let upgrade_types = {
  'astromech': {
    'name': 'XWING.upgrades.astromech',
    'icon': '<i class="xwing-icon">A</i>'
  },
  'bomb': {
    'name': 'XWING.upgrades.bomb',
    'icon': '<i class="xwing-icon">B</i>'
  },
  'cannon': {
    'name': 'XWING.upgrades.cannon',
    'icon': '<i class="xwing-icon">C</i>'
  },
  'crew': {
    'name': 'XWING.upgrades.crew',
    'icon': '<i class="xwing-icon">W</i>'
  },
  'ept': {
    'name': 'XWING.upgrades.ept',
    'icon': '<i class="xwing-icon">E</i>'
  },
  'illicit': {
    'name': 'XWING.upgrades.illicit',
    'icon': '<i class="xwing-icon">I</i>'
  },
  'missile': {
    'name': 'XWING.upgrades.missile',
    'icon': '<i class="xwing-icon">M</i>'
  },
  'modification': {
    'name': 'XWING.upgrades.modification',
    'icon': '<i class="xwing-icon">m</i>'
  },
  'pilot': {
    'name': 'XWING.upgrades.pilot',
    'icon': ''
  },
  'salvaged_astromech': {
    'name': 'XWING.upgrades.salvaged_astromec',
    'icon': '<i class="xwing-icon-1e">V</i>'
  },
  'systems': {
    'name': 'XWING.upgrades.systems',
    'icon': '<i class="xwing-icon">S</i>'
  },
  'title': {
    'name': 'XWING.upgrades.title',
    'icon': '<i class="xwing-icon">t</i>'
  },
  'torpedo': {
    'name': 'XWING.upgrades.torpedo',
    'icon': '<i class="xwing-icon">P</i>'
  },
  'turret': {
    'name': 'XWING.upgrades.turret',
    'icon': '<i class="xwing-icon">U</i>'
  }
}
/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 XWING.abilities = {
  "str": "XWING.AbilityStr",
  "dex": "XWING.AbilityDex",
  "con": "XWING.AbilityCon",
  "int": "XWING.AbilityInt",
  "wis": "XWING.AbilityWis",
  "cha": "XWING.AbilityCha"
};

XWING.abilityAbbreviations = {
  "str": "XWING.AbilityStrAbbr",
  "dex": "XWING.AbilityDexAbbr",
  "con": "XWING.AbilityConAbbr",
  "int": "XWING.AbilityIntAbbr",
  "wis": "XWING.AbilityWisAbbr",
  "cha": "XWING.AbilityChaAbbr"
};

XWING.maneuvers = {
  speeds: ['5','4','3','2','1','0'],
  columns: ['tl','bl','s','br','tr','k','nl','nr']
};

XWING.icons = {
  "attack": '<i class="xwing-icon">%</i>',
  "agility": '<i class="xwing-icon">^</i>',
  "hull": '<i class="xwing-icon">&</i>',
  "shields": '<i class="xwing-icon">*</i>',
  "factions": {
      "imperial": '<i class="xwing-icon">@</i>',
      "rebel": '<i class="xwing-icon">!</i>',
      "scum": '<i class="xwing-icon">#</i>'
  },
  "actions": {
    "barrel_roll": '<i class="xwing-icon">r</i>',
    "boost": '<i class="xwing-icon">b</i>',
    "evade": '<i class="xwing-icon">e</i>',
    "focus": '<i class="xwing-icon">f</i>',
    "target_lock": '<i class="xwing-icon">l</i>'
  },
  "tokens": {
    "stress": '<i class="xwing-icon-1e">ê</i>',
    "focus": '<i class="xwing-icon">ç</i>',
    "evade": '<i class="xwing-icon">é</i>'
  },
  "maneuvers": {
    "tl": '<i class="xwing-icon">4</i>',
    "bl": '<i class="xwing-icon">7</i>',
    "s":  '<i class="xwing-icon">8</i>',
    "br": '<i class="xwing-icon">9</i>',
    "tr": '<i class="xwing-icon">6</i>',
    "k":  '<i class="xwing-icon">2</i>',
    "nl": '<i class="xwing-icon">1</i>',
    "nr": '<i class="xwing-icon">3</i>',
    "stop": '<i class="xwing-icon">5</i>'
  },
  'ships': Object.keys(ship_list).reduce((obj, x) => {obj[x] = ship_list[x].icon; return obj;}, {}),
  'upgrades': Object.keys(upgrade_types).reduce((obj, x) => {obj[x] = upgrade_types[x].icon; return obj;}, {}),
  'faction_helm': {
    'imperial': '<i class="xwing-icon">y</i>',
    'rebel':    '<i class="xwing-icon">x</i>',
    'scum':     '<i class="xwing-icon">z</i>'
  }
}

XWING.ship_names = Object.keys(ship_list).reduce((obj, x) => {obj[x] = ship_list[x].name; return obj;}, {})
XWING.upgrade_names = Object.keys(upgrade_types).reduce((obj, x) => {obj[x] = upgrade_types[x].name; return obj;}, {})

XWING.factions = {
  'imperial': 'XWING.factions.imperial',
  'rebel': 'XWING.factions.rebel',
  'scum': 'XWING.factions.scum'
}