import {executeManeuver, barrelRoll} from './helpers/maneuvers.mjs';

export class ManeuverSelection extends Application {
    constructor(token, action="blank", cust_maneuvers={}, ...args) {
        super(...args);
        this.token = token;
        this.action = action;
        this.cust_maneuvers = cust_maneuvers;
    }
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "";
        options.template = "systems/xwing/templates/maneuvers.html";
        options.height = "auto";
        options.width = 250;
        options.title = "Select Maneuver";
        return options;
    }

    getData(options={}) {
        options.columns = CONFIG.XWING.maneuvers.columns;
        options.speeds = CONFIG.XWING.maneuvers.speeds;
        options.icons = CONFIG.XWING.icons;
        if (this.action == "program") {
            options.show = {
                '3tl': "b",'2tl': "b",'1tl': "b",
                '3bl': "b",'2bl': "b",'1bl': "b",
                '5s':  "b",'4s':  "b",'3s':  "b",'2s': "b",'1s': "b",
                '3br': "b",'2br': "b",'1br': "b",
                '3tr': "b",'2tr': "b",'1tr': "b",
                '5k':  "b",'4k':  "b",'3k':  "b",'2k': "b",'1k': "b",
                '3nl': "b",'2nl': "b",'1nl': "b",
                '3nr': "b",'2nr': "b",'1nr': "b", '0s': 'b', ...this.token.actor.data.data.maneuvers
            };
            options.action_description = "Save";
        } else if (this.action == "assign") {
            options.preselect = this.token?.data?.flags?.xwing?.maneuver?.substring(1) ?? "none";
            options.show = this.token.actor.data.data.maneuvers;
            options.action_description = "Assign";
        } else if (this.action == "view"){
            options.show = this.token.actor.data.data.maneuvers;
            options.action_description = "view";
        } else {
            if (this.action == "custom") { options.show = this.cust_maneuvers; }
            else { options.show = this.token.actor.data.data.maneuvers; }
            options.action_description = "Execute";
        };
        return options;
    }
    activateListeners(html) {
        super.activateListeners(html);

        if (this.action == "program") {
            // Cycle through colors
            html.find('.custom-clickable-maneuver').click( ev => {
                if (ev.currentTarget.classList.contains('b')) {
                    ev.currentTarget.classList.remove('b');
                    ev.currentTarget.classList.add('w');
                } else if (ev.currentTarget.classList.contains('w')) {
                    ev.currentTarget.classList.remove('w');
                    ev.currentTarget.classList.add('g');
                } else if (ev.currentTarget.classList.contains('g')) {
                    ev.currentTarget.classList.remove('g');
                    ev.currentTarget.classList.add('r');                
                } else if (ev.currentTarget.classList.contains('r')) {
                    ev.currentTarget.classList.remove('r');
                    ev.currentTarget.classList.add('b');
                };
            });
            html.find('.execute').click( async ev => {
                let maneuvers = [...html.find(".w, .g, .r")]
                let reshaped = maneuvers.reduce((obj, x) => {
                    obj[x.dataset.maneuver.slice(1)] = x.classList[1];
                    return obj;
                }, {});
                await this.token.actor.update({'data.-=maneuvers': null});
                await this.token.actor.update({"data.maneuvers": reshaped});
                this.close();
            });

        } else if (this.action == "view") {
        } else {
            // Toggle to select one maneuver at a time
            html.find('.custom-clickable-maneuver').click( ev => {
                html.find('td').each((i, td) => { td.classList.remove("select-maneuver"); });
                ev.currentTarget.classList.add("select-maneuver");
            });

            if (this.action == "assign") {
                html.find('.execute').click( ev => {
                    let maneuver = html.find(".select-maneuver")[0].dataset.maneuver;
                    this.token.setFlag("xwing", "maneuver", maneuver);
                    this.close();
                    //executeManeuver(this.token, maneuver);
                });
            } else {
                html.find('.execute').click( ev => {
                    let maneuver = html.find(".select-maneuver")[0].dataset.maneuver;
                    executeManeuver(this.token, maneuver);
                    this.close();
                });
            }
        };

        
    }
}


export class BarrelRollApp extends Application {
    constructor(token, ...args) {
        super(...args);
        this.token = token;
    }
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "";
        options.template = "systems/xwing/templates/barrel-roll.html";
        options.height = "auto";
        options.width = 250;
        options.title = "Barrel Roll";
        return options;
    }

    getData(options={}) {

        return options;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find(".execute").click(async ev =>{
            let slide = parseFloat(html.find('input[name="slide"]').val());
            let lr = html.find('input[name="lr"]:checked').val();

            await barrelRoll(this.token, lr, slide);
            this.close();
        });
    }

}